package cn.tedu.baking.mapper;

import cn.tedu.baking.pojo.entity.User;
import cn.tedu.baking.pojo.vo.UserAdminVO;
import cn.tedu.baking.pojo.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {
    int insert(User user);

    //    @Select("select id, user_name, nick_name, is_admin, create_time, img_url from t_user where user_name = #{name}")
    UserVO selectByUserName(String userName);

    int update(User user);

    //根据id查用户图片路径
    String selectImgUrlById(Long id);

    //管理员查询所有用户信息
    List<UserAdminVO> selectAllUser();

    UserAdminVO selectById(Long id);

    //管理员删除指定普通用户信息功能
    int deleteById(Long id);

    int updateUserById(User user);
}