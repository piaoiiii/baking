package cn.tedu.baking.mapper;

import cn.tedu.baking.pojo.dto.ContentUpdateDTO;
import cn.tedu.baking.pojo.entity.Content;
import cn.tedu.baking.pojo.vo.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ContentMapper {

    //发布稿件（向t_content表插入数据）
    int insertContent(Content content);

    //根据id删除稿件
    int deleteContentById(Long id);

    //根据分类查询稿件内容的查询方法
    List<ContentManagementVO> selectContentByType(Integer type);

    //修改稿件内容
    int update(ContentUpdateDTO content);

    //根据id先查询原稿件数据   (为修改做准备)
    ContentUpdateVO selectUpdateInfoById(Long id);

    /**
     * 根据一级分类和二级分类查询稿件信息
     * @param type       一级分类
     * @param categoryId 二级分类
     * @return 对应的稿件信息
     */
    List<ContentIndexVO> selectContentByTypeAndCategoryId(Integer type, Long categoryId);


    List<ContentManagementVO> selectContentByTypeAndUserId(Integer type, Long userId);

    /**
     * 根据id查询文章详细信息（普通用户点击别人发布的文章）
     * @param id 稿件id
     * @return 稿件详细内容
     */
    ContentDetailVO selectByIdForDetail(Long id);

    /**
     * 根据稿件id修改稿件的浏览量
     *
     * @param id 稿件id
     * @return 修改的记录数
     */
    int updateViewCountById(Long id);

    /**
     * 根据作者id查询作者的其他稿件
     *
     * @param userId    作者id
     * @return 其他稿件信息
     */
    List<ContentSimpleVO> selectByUserId(Long userId);


    //查询热门稿件(访问量最高的前4条)
    List<ContentSimpleVO> selectContentHot();

    /**
     * 首页根据一级分类查询该分类下的所有稿件
     * @param type 一级分类
     * @return 稿件信息
     */
    List<ContentIndexVO> selectListByType(Integer type);

    /**
     * 根据关键字查询稿件信息，关键字搜索查询
     * @param wd 关键字
     * @return
     */
    List<ContentIndexVO> selectContentByWd(String wd);

    /**
     * 管理员界面根据一级分类查询稿件信息
     * @param type 一级分类
     * @return 稿件信息
     */
    List<ContentAdminVO> selectByTypeForAdmin(Integer type);

    /**
     * 根据稿件id修改稿件的评论量
     * @param contentId 稿件id
     * @return 影响的记录数
     */
    int updateCommentCountById(Long contentId);


}

