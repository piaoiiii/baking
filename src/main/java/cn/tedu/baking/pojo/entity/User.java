package cn.tedu.baking.pojo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Long id;
    private String userName; // 用户名
    private String password; // 密码
    private String nickName; // 昵称
    private Date createTime; // 创建时间
    private Integer isAdmin; // 是否是管理员
    private String imgUrl; // 头像路径
}