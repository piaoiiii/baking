package cn.tedu.baking.pojo.entity;

import lombok.Data;
import java.util.Date;

@Data
public class Comment {
    private Long id;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 发表评论者 编号（id）
     */
    private Long userId;
    /**
     * 被评论稿件的编号（id）
     */
    private Long contentId;
    /**
     * 这条评论创建时间
     */
    private Date createTime;
}