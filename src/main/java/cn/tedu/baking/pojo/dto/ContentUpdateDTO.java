package cn.tedu.baking.pojo.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ContentUpdateDTO {
    private Long id;
    private String title;
    private String imgUrl;
    private String videoUrl;
    /**
     * 稿件的一级分类
     * 1 → 食谱
     * 2 → 视频
     * 3 → 咨询
     * 优点: ①节省存储空间 ②查询性能优秀 ③后期维护方便
     */
    private Long type;
    /**
     * 稿件的二级类别
     */
    private Long categoryId;
    private String content;
    private Long updateBy;
    private Date updateTime;
    private String brief;
}
