package cn.tedu.baking.pojo.vo;
import lombok.Data;

import java.util.Date;

@Data
public class BannerAdminVO {
    private Long id;
    /**
     * 轮播图的存储路径
     */
    private String imgUrl;
    /**
     * 创建时间
     */
    private Date createTime;
}