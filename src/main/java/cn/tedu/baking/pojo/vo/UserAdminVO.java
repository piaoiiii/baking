package cn.tedu.baking.pojo.vo;

import lombok.Data;
import java.util.Date;
@Data
public class UserAdminVO {
    private Long id;
    private String userName;
    private String nickName;
    private Date createTime;
    private Integer isAdmin;
    private String imgUrl;
}