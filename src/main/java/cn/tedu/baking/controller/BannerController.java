package cn.tedu.baking.controller;

import cn.tedu.baking.mapper.BannerMapper;
import cn.tedu.baking.pojo.vo.BannerVO;
import cn.tedu.baking.response.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/banners/")
public class BannerController {
    @Autowired
    BannerMapper mapper;

    //首页查询轮播图
    @GetMapping("")
    public JsonResult select() {
        List<BannerVO> list = mapper.select();
        return JsonResult.ok(list);
    }

    //管理界面查询轮播图
    @GetMapping("admin")
    public JsonResult selectForAdmin() {
        return JsonResult.ok(mapper.selectForAdmin());
    }

    @PostMapping("{id}/delete")
    public JsonResult delete(@PathVariable Long id) {
        mapper.deleteById(id);
        return JsonResult.ok();
    }
}