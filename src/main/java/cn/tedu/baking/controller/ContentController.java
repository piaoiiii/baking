package cn.tedu.baking.controller;


import cn.tedu.baking.mapper.ContentMapper;
import cn.tedu.baking.pojo.dto.ContentDTO;
import cn.tedu.baking.pojo.dto.ContentUpdateDTO;
import cn.tedu.baking.pojo.entity.Content;
import cn.tedu.baking.pojo.vo.ContentIndexVO;
import cn.tedu.baking.pojo.vo.ContentUpdateVO;
import cn.tedu.baking.pojo.vo.UserVO;
import cn.tedu.baking.response.JsonResult;
import cn.tedu.baking.response.StatusCode;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/contents/")
public class ContentController {
    @Autowired
    ContentMapper mapper;

    @PostMapping("add-new")
    public JsonResult addNew(@RequestBody ContentDTO contentDTO) {
        Content content = new Content();
        BeanUtils.copyProperties(contentDTO, content);
        content.setCreateTime(new Date());
        mapper.insertContent(content);
        return JsonResult.ok();
    }


    @GetMapping("/{type}/management")
    public JsonResult management(@PathVariable Integer type, HttpSession session){
        UserVO user = (UserVO)session.getAttribute("user");
        if (user==null){
            //用户未登录
            return new JsonResult(StatusCode.NOT_LOGIN);
        }
        //查询文章的列表---查当前登录用户的
        //+查询条件  当前登录用户的id
        return JsonResult.ok(mapper.selectContentByTypeAndUserId(type,user.getId()));
    }


    @PostMapping("{id}/update")
    public JsonResult rUpdate(@RequestBody ContentUpdateDTO contentUpdateDTO) {
        return JsonResult.ok(mapper.update(contentUpdateDTO));
    }

    @GetMapping("{id}/selectForUpdate")
    public JsonResult selectForUpdate(@PathVariable Long id) {
        return JsonResult.ok(mapper.selectUpdateInfoById(id));
    }


    @PostMapping("{id}/delete")
    public JsonResult delete(@PathVariable Long id) {
        //得到封面图片路径并删除
        ContentUpdateVO contentUpdateVO = mapper.selectUpdateInfoById(id);
        new File("d:/file" + contentUpdateVO.getImgUrl()).delete();
        if (contentUpdateVO.getType() == 2) {//判断如果是视频则删除视频文件
            new File("d:/file" + contentUpdateVO.getVideoUrl()).delete();
        }
        mapper.deleteContentById(id);
        return JsonResult.ok();
    }

    @GetMapping("{type}/{categoryId}/index")
    public JsonResult selectIndex(@PathVariable Integer type,
                                  @PathVariable Long categoryId) {
        System.out.println("type = " + type + ", categoryId = " + categoryId);
        List<ContentIndexVO> list = mapper.selectContentByTypeAndCategoryId(type, categoryId);
        return JsonResult.ok(list);
    }

    @GetMapping("{id}/detail")
    public JsonResult selectDetail(@PathVariable Long id) {
        //让浏览量+1
        mapper.updateViewCountById(id);
        return JsonResult.ok(mapper.selectByIdForDetail(id));
    }

    @GetMapping("{userId}/other")
    public JsonResult selectOther(@PathVariable Long userId) {
        return JsonResult.ok(mapper.selectByUserId(userId));
    }

    @GetMapping("hot")
    public JsonResult selectHot() {
        return JsonResult.ok(mapper.selectContentHot());
    }

    @GetMapping("{type}/list")
    public JsonResult selectList(@PathVariable Integer type) {
        return JsonResult.ok(mapper.selectListByType(type));
    }

    @GetMapping("{wd}/search")
    public JsonResult search(@PathVariable String wd) {
        return JsonResult.ok(mapper.selectContentByWd(wd));
    }

    @GetMapping("{type}/admin")
    public JsonResult selectAdmin(@PathVariable Integer type) {
        return JsonResult.ok(mapper.selectByTypeForAdmin(type));
    }

}