package cn.tedu.baking.controller;

import cn.tedu.baking.mapper.UserMapper;
import cn.tedu.baking.pojo.dto.UserLoginDTO;
import cn.tedu.baking.pojo.dto.UserRegDTO;
import cn.tedu.baking.pojo.dto.UserUpdateDTO;
import cn.tedu.baking.pojo.entity.User;
import cn.tedu.baking.pojo.vo.UserAdminVO;
import cn.tedu.baking.pojo.vo.UserVO;
import cn.tedu.baking.response.JsonResult;
import cn.tedu.baking.response.StatusCode;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/users/")
public class UserController {
    @Autowired
    private UserMapper mapper;

    @PostMapping("reg")
    public JsonResult reg(@RequestBody UserRegDTO userRegDTO) {
        UserVO userVO = mapper.selectByUserName(userRegDTO.getUserName());
        if (userVO != null) {
            return new JsonResult(StatusCode.USERNAME_ALREADY_EXISTS);
        }
        User user = new User();
        BeanUtils.copyProperties(userRegDTO, user);
        user.setCreateTime(new Date());
        mapper.insert(user);
        return JsonResult.ok();
    }

    @PostMapping("login")
    public JsonResult login(@RequestBody UserLoginDTO userLoginDTO, HttpSession session) {
        UserVO userVO = mapper.selectByUserName(userLoginDTO.getUserName());
        if (userVO == null) {
            return new JsonResult(StatusCode.USERNAME_ERROR);
        }
        if (!userVO.getPassword().equals(userLoginDTO.getPassword())) {
            return new JsonResult(StatusCode.PASSWORD_ERROR);
        }
        //把当前登录的用户对象保存到会话对象中
        session.setAttribute("user", userVO);
        return JsonResult.ok(userVO);
    }

    @GetMapping("logout")
    public JsonResult logout(HttpSession session) {
        session.removeAttribute("user");
        return JsonResult.ok();
    }

    @PostMapping("update")
    public JsonResult update(@RequestBody UserUpdateDTO userUpdateDTO) {
        //判断出是否需要更换头像
        if (userUpdateDTO.getImgUrl() != null) {
            //得到原图片路径
            String imgUrl = mapper.selectImgUrlById(userUpdateDTO.getId());
            //删除对应的图片文件
            new File("d:/file" + imgUrl).delete();
        }
        User user = new User();
        BeanUtils.copyProperties(userUpdateDTO, user);
        mapper.updateUserById(user);
        return JsonResult.ok();
    }

    //获取所有用户
    @GetMapping("")
    public JsonResult list() {
        List<UserAdminVO> userAdminVOS = mapper.selectAllUser();
        for (UserAdminVO uav : userAdminVOS) {
            System.out.println(uav.toString());

        }
        return JsonResult.ok(userAdminVOS);
    }

    @PostMapping("{id}/{isAdmin}/change")
    public JsonResult change(@PathVariable Long id,
                             @PathVariable Integer isAdmin) {
        User user = new User();
        user.setId(id);
        user.setIsAdmin(isAdmin);
        mapper.updateUserById(user);
        return JsonResult.ok();
    }

    @PostMapping("{id}/delete")
    public JsonResult delete(@PathVariable Long id) {
        mapper.deleteById(id);
        return JsonResult.ok();
    }

    @GetMapping("{id}")
    public JsonResult getUserInfoById(@PathVariable Long id, HttpSession session) {
        UserVO userVo = (UserVO) session.getAttribute("user");
        if (userVo != null && userVo.getId() == id){
            UserAdminVO userAdminVO = mapper.selectById(id);
            return JsonResult.ok(userAdminVO);
        } else {
            return new JsonResult(StatusCode.FORBIDDEN_ERROR);
        }
    }
}
