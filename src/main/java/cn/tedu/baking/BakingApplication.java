package cn.tedu.baking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan// 此注解,用于开启扫描当前类包下的过滤器类
public class BakingApplication {

    public static void main(String[] args) {
        SpringApplication.run(BakingApplication.class, args);
    }

}
