package cn.tedu.baking.pojo.entity;


import cn.tedu.baking.mapper.CommentMapper;
import cn.tedu.baking.mapper.ContentMapper;
import cn.tedu.baking.pojo.entity.Comment;
import cn.tedu.baking.pojo.entity.Content;
import cn.tedu.baking.pojo.vo.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class CommentTests {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ContentMapper contentMapper;

    @Test
    public void insert() {//id为3的用户给稿件id为16的评论
        Comment comment = new Comment();
        comment.setContent("aa真的是太棒了!!!我一定要学会");//评论内容
        comment.setUserId(3L);//发表评论的人的id
        comment.setContentId(16L);//被评论的文章id
        comment.setCreateTime(new Date());//评论创建时间
        int rows = commentMapper.insert(comment);
        System.out.println(rows > 0 ? "发表评论成功" : "发表评论失败!");
        //让评论量增加1
        if (rows > 0) {//如果评论发表成功，评论量+1
            //让稿件的评论量+1
            contentMapper.updateCommentCountById((long)comment.getContentId());
            System.out.println("评论成功!,评论量加一");
        }
    }

    @Test
    public void selectByContentId() {//查询文章下所有的评论
        List<CommentVO> commentVOS = commentMapper.selectCommentByContentId(16L);
        for (CommentVO commentVO : commentVOS) {
            System.out.println(commentVO);
        }
    }

}