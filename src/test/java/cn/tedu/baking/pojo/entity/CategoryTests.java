package cn.tedu.baking.pojo.entity;

import cn.tedu.baking.mapper.CategoryMapper;
import cn.tedu.baking.pojo.vo.CategoryVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CategoryTests {
    @Autowired
    private CategoryMapper categoryMapper;

    @Test
    public void selectByType(){
        List<CategoryVO> categoryVOS = categoryMapper.selectByType(1);
        for (CategoryVO cv : categoryVOS) {
            System.out.println(cv.toString());
        }
    }
}
