//package cn.tedu.baking.pojo.entity;
//
//import cn.tedu.baking.mapper.UserMapper;
//import cn.tedu.baking.pojo.vo.UserAdminVO;
//import cn.tedu.baking.pojo.vo.UserVO;
//import javafx.application.Application;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.core.io.Resource;
//import org.springframework.core.io.ResourceLoader;
//import org.springframework.util.ClassUtils;
//
//import java.io.File;
//import java.util.Date;
//import java.util.List;
//
//@SpringBootTest
//public class UserTests {
//    @Autowired
//    private UserMapper userMapper;
//
//    @Test
//    public void insert() {
//        User user = new User();
//        user.setUserName("小明");
//        user.setNickName("明明");
//        user.setPassword("123456");
//        user.setCreateTime(new Date());
//
//        UserVO userVO = userMapper.selectByUserName(user.getUserName());
//        if (userVO != null){
//            System.out.println("用户名已被占用,请更换用户名重新注册!");
//            return;
//        }
//        int rows = userMapper.insert(user);
//        System.out.println(rows > 0 ? "注册成功!" : "注册失败!");
//    }
//
//    @Test
//    public void selectUserByName(){
//        UserVO userVO = userMapper.selectByUserName("tom");
//        System.out.println(userVO.toString());
//    }
//
//    @Test
//    public void update(){
//        User user = new User();
//        user.setId(2L);
//        user.setNickName("小明明1");
////        user.setImgUrl("d:/1.jpg");
//        System.out.println(user);
//        int rows = userMapper.update(user);
//        System.out.println(rows > 0 ? "修改成功!" : "修改失败!");
//    }
//
//    //根据id查用户头像图片路径的测试类
//    @Test
//    public void selectImgUrlById() {
//        String url = userMapper.selectImgUrlById(2L);
//        System.out.println("用户头像图片的存储路径:"+url);
//    }
//
//    //修改用户头像，先判断原来有没有头像路径，有删掉原来的，再添加新的
//    @Test
//    public void update1() {
//        String imgPath = "./pic/1.jpg";
//        User user = new User();
//        user.setId(1L);
//        String imgUrl = userMapper.selectImgUrlById(user.getId());
//        String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
//        System.out.println(path);
//        if (imgUrl != null) {//如果有头像（根据有没有储存路径判定）
//            File f = new File(imgPath);
//            System.out.println(f.getAbsolutePath());
//            System.out.println(f.getPath());
//            System.out.println();
////            new File(imgUrl).delete();//将用户的原头像删除
//            System.out.println("原用户头像已删除");
//        }
////        user.setImgUrl(imgPath);//重新设置用户头像图片所在路径
////        int rows = userMapper.update(user);
////        System.out.println(rows > 0 ? "修改成功!" : "修改失败!");
//    }
//
//    //管理员查寻所有用户信息 功能测试
//    @Test
//    public void select() {
//        List<UserAdminVO> list = userMapper.selectAllUser();
//        for (UserAdminVO userAdminVO : list) {
//            System.out.println(userAdminVO);
//        }
//    }
//
//    //管理员删除指定普通用户信息功能 测试
//    @Test
//    public void delete() {
//        int rows = userMapper.deleteById(1L);
//        System.out.println(rows > 0 ? "删除成功!" : "删除失败!");
//    }
//}