package cn.tedu.baking.pojo.entity;

import cn.tedu.baking.mapper.BannerMapper;
import cn.tedu.baking.pojo.vo.BannerAdminVO;
import cn.tedu.baking.pojo.vo.BannerVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class BannerTests {
    @Autowired
    private BannerMapper bannerMapper;
    @Test
    public void select() {//首页轮播图查询显示
        List<BannerVO> select = bannerMapper.select();
        for (BannerVO banner : select) {
            System.out.println(banner);
        }
    }

    @Test
    public void selectForAdmin() {//管理员用户查询所有轮播图
        List<BannerAdminVO> bannerAdminVOS = bannerMapper.selectForAdmin();
        for (BannerAdminVO bannerAdminVO : bannerAdminVOS) {
            System.out.println(bannerAdminVO);
        }
    }

    @Test
    public void deleteById() {//根据id删除轮播图
        int rows = bannerMapper.deleteById(4L);
        System.out.println(rows > 0 ? "删除轮播图成功" : "删除轮播图失败");
    }
}
