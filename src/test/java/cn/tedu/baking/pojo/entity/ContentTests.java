package cn.tedu.baking.pojo.entity;

import cn.tedu.baking.mapper.ContentMapper;
import cn.tedu.baking.pojo.dto.ContentUpdateDTO;
import cn.tedu.baking.pojo.vo.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class ContentTests {
    //测试发布稿件功能（向t_content表插入数据）
    @Autowired
    private ContentMapper contentMapper;
    @Test
    public void insert() {
        Content content = new Content();
        content.setTitle("测试文章");     //稿件标题
        content.setImgUrl("./1.jpg");   //稿件封面(存储的是图片的路径)
        content.setVideoUrl("./2.mp4"); // 视频(存储的是视频的路径)
        content.setContent("文章正文内容");//稿件正文
        content.setType(2L);     //稿件的一级分类 1 → 食谱、2 → 视频、3 → 咨询
        content.setCreateBy(4L);  //稿件作者编号(取自于t_user表id)
        content.setCreateTime(new Date()); //创建时间
        content.setBrief("文章正文的前50个字");  //摘要
        content.setCategoryId(1L);          //稿件的二级类别
        int rows = contentMapper.insertContent(content);
        System.out.println(rows > 0 ? "发布成功!" : "发布失败!");
    }

    //根据分类查询稿件内容 测试
    @Test
    public void selectByType() {
        //type → 1:食谱 2:视频 3:咨询
        List<ContentManagementVO> contentManagementVOS = contentMapper.selectContentByType(2);
        for (ContentManagementVO contentManagementVO : contentManagementVOS) {
            System.out.println(contentManagementVO);
        }
    }

    //根据id删除稿件测试
    @Test
    public void deleteById() {
        int rows = contentMapper.deleteContentById(27L);
        System.out.println(rows > 0 ? "删除成功" : "删除失败");
    }

    //根据id修改稿件内容
    @Test
    public void update() {
        ContentUpdateDTO content = new ContentUpdateDTO();
        content.setId(11L);  //被修改稿件id
        content.setTitle("不买亏死的花生酱酥饼");
        content.setUpdateBy(12L); //更新者ID
        content.setUpdateTime(new Date()); //更新时间
        int rows = contentMapper.update(content);
        System.out.println(rows > 0 ? "修改成功" : "修改失败");
    }

    @Test
    public void selectByIdForUpdate() {
        ContentUpdateVO contentUpdateVO = contentMapper.selectUpdateInfoById(11L);
        System.out.println(contentUpdateVO);
    }

    //type：一级类型（视频、食谱、咨询）
    //categoryId:二级类别id
    @Test
    public void selectByTypeAndCategoryId() {
        List<ContentIndexVO> contentIndexVOS = contentMapper.selectContentByTypeAndCategoryId(1, 2L);
        for (ContentIndexVO contentIndexVO : contentIndexVOS) {
            System.out.println(contentIndexVO);
        }
    }

//    //根据id查询文章详细信息（普通用户点击别人发布的文章）
//    @Test
//    public void selectByIdForDetail() {
//        ContentDetailVO contentDetailVO = contentMapper.selectByIdForDetail(11L);
//        System.out.println(contentDetailVO);
//    }

    @Test
    public void updateViewCountById() {//修改访问量的方法
        int rows = contentMapper.updateViewCountById(11L);
        System.out.println(rows > 0 ? "访问量修改成功" : "访问量修改失败!");
    }

    //根据id查询文章详细信息（普通用户点击别人发布的文章），
    //并添加浏览量
    @Test
    public void selectByIdForDetail() {
        contentMapper.updateViewCountById(11L); //浏览量+1
        ContentDetailVO contentDetailVO = contentMapper.selectByIdForDetail(11L);
        System.out.println(contentDetailVO);
    }

    //添加查询作者其他的文章功能，t_content表中的create_by（文章创建者）作者的id
    @Test
    public void selectByUserId() {
        List<ContentSimpleVO> contentSimpleVOS = contentMapper.selectByUserId(2L);
        for (ContentSimpleVO contentSimpleVO : contentSimpleVOS) {
            System.out.println(contentSimpleVO);
        }
    }

    @Test
    public void selectHot() {//查询热门稿件(访问量最高的前4条)
        List<ContentSimpleVO> contentSimpleVOS = contentMapper.selectContentHot();
        for (ContentSimpleVO contentSimpleVO : contentSimpleVOS) {
            System.out.println(contentSimpleVO);
        }
    }

    @Test
    public void selectListByType() {//首页根据一级分类查询该分类下的所有稿件
        List<ContentIndexVO> contentIndexVOS = contentMapper.selectListByType(1);
        for (ContentIndexVO contentIndexVO : contentIndexVOS) {
            System.out.println(contentIndexVO);
        }
    }

    @Test
    public void selectByWd() {//关键字查询
        List<ContentIndexVO> contentIndexVOS = contentMapper.selectContentByWd("花生酱酥饼");
        for (ContentIndexVO contentIndexVO : contentIndexVOS) {
            System.out.println(contentIndexVO);
        }
    }

    @Test
    public void selectByTypeForAdmin() {//管理员界面根据一级分类查询稿件信息（为后续修改做准备）
        List<ContentAdminVO> contentAdminVOS = contentMapper.selectByTypeForAdmin(1);
        for (ContentAdminVO contentAdminVO : contentAdminVOS) {
            System.out.println(contentAdminVO);
        }
    }

    //根据稿件id修改稿件的评论量
    @Test
    public void updateCommentCountById() {
        int rows = contentMapper.updateCommentCountById(16L);
        System.out.println(rows > 0 ? "修改成功" : "修改失败");
    }

}
